.. meta::
   :description:
        Canvas input settings in Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
   :license: GNU free documentation license 1.3 or later.

.. index:: Preferences, Settings, Tablet, Canvas Input Settings
.. _canvas_input_settings:

=====================
Canvas Input Settings
=====================

Krita has ways to set mouse and keyboard combinations for different actions. The user can set which combinations to use for a certain Krita command over here. This section is under development and will include more options in future.

Profile
    The user can make different profiles of combinations and save them.

Tool Invocation
    Activate with Other Color
        Same as Activate, but with the tool treating the foreground and background colors as if they were flipped.
